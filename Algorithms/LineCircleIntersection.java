import java.util.Scanner;

public class LineCircleIntersection {
	private static class Point {
		double x, y;
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		public void print(Point parallelMove) {
			System.out.println("" + (x + parallelMove.x) + " " + (y + parallelMove.y));
		}
	}
	private final static double eps = 1e-8;
	private static double sqr(double x) {
		return x * x;
	}
	private static boolean eq0(double x) {
		return Math.abs(x) < eps;
	}
	private static boolean ls0(double x) {
		return x < -eps;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double r, a, b, c;
		Point circleCenter = new Point(scanner.nextDouble(), scanner.nextDouble());
		r = scanner.nextDouble();
		a = scanner.nextDouble();
		b = scanner.nextDouble();
		c = scanner.nextDouble();
		c = a * circleCenter.x + b * circleCenter.y + c;
		double squaredNormalVectorLength = sqr(a) + sqr(b);
		double normalVectorLength = Math.sqrt(squaredNormalVectorLength);
		Point closestLinePoint = new Point(
			-a * c / squaredNormalVectorLength,
			-b * c / squaredNormalVectorLength
		);
		double distance = Math.abs(c) / normalVectorLength;
		if(eq0(distance - r)) { // distance == r
			// one intersection point
			System.out.println(1);
			closestLinePoint.print(circleCenter);
		} else if(ls0(distance - r)) { // distance - r < 0 <=> distance < r
			// two intersection points
			System.out.println(2);
			double d = Math.sqrt(sqr(r) - (sqr(c) / squaredNormalVectorLength));
			double coeff = Math.sqrt(sqr(d)  / squaredNormalVectorLength);
			Point firstCross = new Point(
				closestLinePoint.x - b * coeff,
				closestLinePoint.y + a * coeff
			), secondCross = new Point(
				closestLinePoint.x + b * coeff,
				closestLinePoint.y - a * coeff
			);
			firstCross.print(circleCenter);
			secondCross.print(circleCenter);
		} else {
			// no intersection points
			System.out.println(0);
		}		
	}
}