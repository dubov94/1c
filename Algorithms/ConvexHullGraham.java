import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;


public class ConvexHullGraham {
	static class Point {
		private double sqr(double t) {
			return t * t;
		}
		double x, y;
		Point(Scanner scanner) {
			x = scanner.nextDouble();
			y = scanner.nextDouble();
		}
		Point(Point other) {
			x = other.x;
			y = other.y;
		}
		Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		void move(Point oppositeDirection) {
			x -= oppositeDirection.x;
			y -= oppositeDirection.y;
		}
		double dist() {
			return Math.sqrt(sqr(x) + sqr(y));
		}
		double dist(Point other) {
			return Math.sqrt(sqr(other.x - x) + sqr(other.y - y));
		}
	}
	
	static int mostLeftBottomIndex(Point[] points) {
		int answerIndex = 0;
		for(int i = 1; i < points.length; ++i) {
			if(points[i].y < points[answerIndex].y) {
				answerIndex = i;
			} else if(points[i].y == points[answerIndex].y && points[i].x < points[answerIndex].x) {
				answerIndex = i;
			}
		}
		return answerIndex; 
	}
	static double crossProduct(Point a, Point b) {
		return a.x * b.y - a.y * b.x;
	}
	static boolean leftTurn(Point a, Point b, Point c) {
		Point v1 = new Point(b.x - a.x, b.y - a.y);
		Point v2 = new Point(c.x - b.x, c.y - b.y);
		return crossProduct(v1, v2) > 0;
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		Point[] points = new Point[n];
		for(int i = 0; i < n; ++i) {
			points[i] = new Point(scanner);
		}
		int leftBotIndex = mostLeftBottomIndex(points);
		Point leftBotPoint = new Point(points[leftBotIndex]);
		for(int i = 0; i < n; ++i) {
			points[i].move(leftBotPoint);
		}
		points[leftBotIndex] = points[0];
		points = Arrays.copyOfRange(points, 1, points.length);
		Arrays.sort(points, new Comparator<Point>() {
			@Override
			public int compare(Point a, Point b) {
				double product = crossProduct(a, b);
				if(product > 0) { // a has less angle than b => a < b
					return -1;
				} else if(product < 0) { // a > b
					return 1;
				} else {
					double distA = a.dist(), distB = b.dist();
					if(distA < distB) return -1;
					else if(distA > distB) return 1;
					else return 0;
				}
			}
		});
		Point[] stack = new Point[n];
		stack[0] = new Point(0, 0);
		stack[1] = points[0];
		int stackSize = 2;
		for(int i = 1; i < points.length; ++i) {
			while(stackSize >= 2 && !leftTurn(stack[stackSize - 2], stack[stackSize - 1], points[i])) {
				--stackSize;
			}
			stack[stackSize++] = points[i];
		}
		double perimeter = 0, square = 0;
		for(int i = 1; i < stackSize; ++i) {
			square += (stack[i].x - stack[i - 1].x) * (stack[i].y + stack[i - 1].y);
			perimeter += stack[i].dist(stack[i - 1]);
		}
		square += (stack[0].x - stack[stackSize - 1].x) * (stack[0].y + stack[stackSize - 1].y);
		square /= 2;
		perimeter += stack[0].dist(stack[stackSize - 1]);
		System.out.println(perimeter);
		System.out.println(-square);
	}
}
