#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

struct Point {
    double x, y;

    Point() {
        cin >> x >> y;
    }

    Point(double x, double y) : x(x), y(y) {
    }

    double crossProduct(const Point &other) const {
        return x * other.y - y * other.x;
    }

    void move(Point &oppositeDirection) {
        x -= oppositeDirection.x;
        y -= oppositeDirection.y;
    }

    double dist() const {
        return sqrt(sqr(x) + sqr(y));
    }

    double dist(const Point &other) const {
        return sqrt(sqr(other.x - x) + sqr(other.y - y));
    }

    bool operator<(const Point &other) const {
        double product = crossProduct(other);
        if (product > 0) {
            return true;
        } else if (product == 0 && this->dist() < other.dist()) {
            return true;
        }
        return false;
    }

private:
    double sqr(double t) const {
        return t * t;
    }
};

int mostLeftBottomIndex(vector<Point> &points) {
    int answerIndex = 0;
    for (int i = 1; i < points.size(); ++i) {
        if (points[i].y < points[answerIndex].y) {
            answerIndex = i;
        } else if (points[i].y == points[answerIndex].y && points[i].x < points[answerIndex].x) {
            answerIndex = i;
        }
    }
    return answerIndex;
}

bool leftTurn(Point &a, Point &b, Point &c) {
    Point v1(b.x - a.x, b.y - a.y);
    Point v2(c.x - b.x, c.y - b.y);
    return v1.crossProduct(v2) > 0;
}

int main() {
    int n;
    cin >> n;
    vector<Point> points(n);
    int leftBotIndex = mostLeftBottomIndex(points);
    Point leftBotPoint(points[leftBotIndex]);
    for (int i = 0; i < n; ++i) {
        points[i].move(leftBotPoint);
    }
    points[leftBotIndex] = *points.rbegin();
    points.pop_back();
    sort(points.begin(), points.end());
    vector<Point> stack;
    stack.push_back(Point(0, 0));
    stack.push_back(points[0]);
    for (int i = 1; i < points.size(); ++i) {
        while (stack.size() >= 2 && !leftTurn(stack[stack.size() - 2], stack[stack.size() - 1], points[i])) {
            stack.pop_back();
        }
        stack.push_back(points[i]);
    }
    int stackSize = stack.size();
    double perimeter = 0, square = 0;
    for (int i = 1; i < stackSize; ++i) {
        square += (stack[i].x - stack[i - 1].x) * (stack[i].y + stack[i - 1].y);
        perimeter += stack[i].dist(stack[i - 1]);
    }
    square += (stack[0].x - stack[stackSize - 1].x) * (stack[0].y + stack[stackSize - 1].y);
    square /= 2;
    perimeter += stack[0].dist(stack[stackSize - 1]);
    cout << perimeter << endl << -square;
    return 0;
}