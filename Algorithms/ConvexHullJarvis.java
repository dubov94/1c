import java.util.Scanner;


public class ConvexHullJarvis {
	static class Point {
		int x, y;
		public Point(Scanner scanner) {
			x = scanner.nextInt();
			y = scanner.nextInt();
		}
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
		public void print() {
			System.out.println(x + " " + y);
		}
	}
	
	static int findMostLeftBot(Point[] points) {
		int minIndex = 0;
		for(int i = 1; i < points.length; ++i) {
			if(points[i].y < points[minIndex].y) {
				minIndex = i;
			} else if(points[i].y == points[minIndex].y
					&& points[i].x < points[minIndex].x) {
				minIndex = i;
			}
		}
		return minIndex;
	}
	static int crossProduct(Point begin, Point end1, Point end2) {
		Point v1 = new Point(end1.x - begin.x, end1.y - begin.y);
		Point v2 = new Point(end2.x - begin.x, end2.y - begin.y);
		return v1.x * v2.y - v1.y * v2.x;
	}
	static int sqr(int x) {
		return x * x;
	}
	static int dist2(Point first, Point second) {
		return sqr(second.x - first.x) + sqr(second.y - first.y);
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		Point[] points = new Point[n];
		for(int i = 0; i < n; ++i) {
			points[i] = new Point(scanner);
		}
		int mostLeftBot = findMostLeftBot(points);
		Point[] hull = new Point[n + 1];
		int hullSize = 0;
		hull[hullSize++] = points[mostLeftBot];
		do {
			int minAnglePointIndex = 0;
			Point begin = hull[hullSize - 1];
			for(int i = 0; i < n; ++i) {
				int product = crossProduct(begin, points[minAnglePointIndex], points[i]);
				if(product < 0) {
					minAnglePointIndex = i;
				} else if(product == 0 
						&& dist2(begin, points[minAnglePointIndex]) < dist2(begin, points[i])) {
					minAnglePointIndex = i;
				}
			}
			hull[hullSize++] = points[minAnglePointIndex];
		} while(hull[hullSize - 1] != points[mostLeftBot]);
		int answerSize = hullSize - 1;
		System.out.println(answerSize);
		if(n % 2 == 0) {
			for(int i = 0; i < answerSize; ++i) {
				hull[i].print();
			}
		} else {
			for(int i = answerSize - 1; i >= 0; --i) {
				hull[i].print();
			}
		}
	}
}
