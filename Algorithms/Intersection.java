import java.util.Locale;
import java.util.Scanner;


public class Intersection {
	public static class Point {
		public long x, y;
		public void read(Scanner scanner) {
			x = scanner.nextInt();
			y = scanner.nextInt();
		}
	}
	public static class Line {
		public long a, b, c;
		public Line(Point p1, Point p2) {
			a = p2.y - p1.y;
			b = p1.x - p2.x;
			c = -(a * p1.x + b * p1.y);
		}
		public boolean check(Point p) {
			return a * p.x + b * p.y + c == 0;
		}
	}
	
	public static void halt(int result) {
		System.out.println(result);
		System.exit(0);
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		scanner.useLocale(Locale.US);
		Point p1 = new Point(), p2 = new Point(), p3 = new Point(), p4 = new Point();
		p1.read(scanner);
		p2.read(scanner);
		p3.read(scanner);
		p4.read(scanner);
		Line l1 = new Line(p1, p2);
		Line l2 = new Line(p3, p4);
		if(l1.a * l2.b == l1.b * l2.a) {
			if(l1.a * l2.c == l1.c * l2.a && l1.b * l2.c == l1.c * l2.b) {
				halt(2);
			} else {
				halt(0);
			}
		} else {
			double num = l1.c * l2.b - l2.c * l1.b;
			double den = l1.a * l2.b - l2.a * l1.b;
			double x = -num / den;
			num = l1.a * l2.c - l2.a * l1.c;
			den = l1.a * l2.b - l2.a * l1.b;
			double y = -num / den;
			System.out.println(1 + " " + x + " " + y);
		}
	}
}
