import java.util.Scanner;


public class Classification {
	public static class Line {
		public long a, b, c;
		public Line(Scanner scanner) {
			a = scanner.nextInt();
			b = scanner.nextInt();
			c = scanner.nextInt();
		}
		public long calculate(Point p) {
			return a * p.x + b * p.y + c;
		}
	}
	public static class Point {
		public long x, y;
		public Point(Scanner scanner) {
			x = scanner.nextInt();
			y = scanner.nextInt();
		}
	}
	public static int sign(long x) {
		if(x > 0) return 1;
		if(x < 0) return -1;
		return 0;
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Point p1 = new Point(scanner), p2 = new Point(scanner);
		Line l = new Line(scanner);
		System.out.println(sign(l.calculate(p1)) == sign(l.calculate(p2)) ? "YES": "NO");
	}
}
