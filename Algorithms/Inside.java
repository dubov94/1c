import java.util.Locale;
import java.util.Scanner;

public class Inside {
	public static final double eps = 1e-6;
	public static boolean eq0(double x) {
		return Math.abs(x) < eps;
	}
	public static boolean gr0(double x) {
		return x > eps;
	}
	public static boolean ls0(double x) {
		return x < -eps;
	}
	public static class Point {
		public Point(Scanner scanner) {
			x = scanner.nextDouble();
			y = scanner.nextDouble();
		}
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
		public double x, y;
	}
	public static class Segment {
		public double a, b, c;
		private Point lowerBound, upperBound;
		public Segment(Point A, Point B) {
			b = A.x - B.x;
			a = B.y - A.y;
			c = -(a * A.x + b * A.y);
			lowerBound = new Point(Math.min(A.x, B.x), Math.min(A.y, B.y));
			upperBound = new Point(Math.max(A.x, B.x), Math.max(A.y, B.y));
		}
		public boolean contains(Point N) {
			return eq0(calculate(N))
					&& N.y <= upperBound.y && N.y >= lowerBound.y
					&& N.x <= upperBound.x && N.x >= lowerBound.x;
		}
		public double calculate(Point N) {
			return a * N.x + b * N.y + c;
		}
	}
	public static int intersect(Point A, Point B) {
		Segment segment = new Segment(A, B);
		if(segment.contains(M)) {
			// case: M belongs to [A; B]
			return -1;
		} else {
			if(gr0(A.y * B.y)) {
				// case: [A; B] is completely above or below OX
				return 0;
			} else if(eq0(A.y * B.y)) {
				// the point with y = 0 has to have on the beam to be considered
				if(eq0(A.y) && gr0(A.x)
						|| eq0(B.y) && gr0(B.x)) {
					// wikipedia: 
					// "If the intersection point is a vertex of a tested polygon side, 
					// then the intersection counts only if the second vertex of the side lies below the ray."
					if(eq0(A.y)) {
						if(ls0(B.y)) return 1;
					} else {
						if(ls0(A.y)) return 1;
					}
					return 0;
				} else {
					return 0;
				}
			} else {
				if(gr0(-segment.c / segment.a)) {
					// a * x + b * 0 + c = 0, a != 0 because a.y != b.y
					return 1;
				} else {
					return 0;
				}
			}
		}
	}
	public static Point M;
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		scanner.useLocale(Locale.US);
		int n = scanner.nextInt();
		Point[] polygon = new Point[n];
		for(int i = 0; i < n; ++i) {
			polygon[i] = new Point(scanner);
		}
		M = new Point(scanner);
		for(int i = 0; i < n; ++i) {
			polygon[i] = new Point(polygon[i].x - M.x, polygon[i].y - M.y);
		}
		M = new Point(0, 0);
		int crosses = 0;
		for(int i = 0; i < n; ++i) {
			Point A = polygon[i], B = polygon[i + 1 >= n ? 0 : i + 1];
			int result = intersect(A, B);
			if(result == -1) {
				System.out.println("YES");
				return;
			} else {
				crosses += result;
			}
		}
		if(crosses % 2 == 0) {
			System.out.println("NO");
		} else {
			System.out.println("YES");
		}
	}
}
