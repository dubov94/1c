import java.util.Scanner;

public class Heapify {
	static class Heap {
		int[] tree;
		int size;
		public Heap(int n) {
			tree = new int[n + 1];
			size = 0;
		}
		private void swap(int i, int j) {
			int t = tree[i];
			tree[i] = tree[j];
			tree[j] = t;
		}
		public void insert(int x) {
			tree[++size] = x;
			int v = size;
			while(v / 2 > 0) {
				int parent = v / 2;
				if(tree[parent] < tree[v]) {
					swap(parent, v);
					v = parent;
				} else {
					break;
				}
			}
		}
		public int extract() {
			int ret = tree[1];
			tree[1] = tree[size--];
			int v = 1;
			while(v * 2 <= size) { // tree[v] is not a leaf
				int leftChild = v * 2, rightChild = v * 2 + 1;
				int maxIndex = v;
				if(tree[maxIndex] < tree[leftChild]) {
					maxIndex = leftChild;
				}
				if(rightChild <= size
						&& tree[maxIndex] < tree[rightChild]) {
					maxIndex = rightChild;
				}
				if(maxIndex == v) {
					break;
				} else {
					swap(maxIndex, v);
					v = maxIndex;
				}
			}
			return ret;
		}
	}
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		Heap heap = new Heap(n);
		for(int i = 0; i < n; ++i) {
			int operation = scanner.nextInt();
			if(operation == 0) {
				heap.insert(scanner.nextInt());
			} else {
				System.out.println(heap.extract());
			}
		}
	}
}
