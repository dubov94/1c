package tetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class View extends JPanel implements Observer {
	private final int resolution = 20, border = 1;
	private final Model model;

	public View(Model model) {
		this.model = model;
		setPreferredSize(new Dimension(model.getWidth() * resolution,
				model.getHeight() * resolution));
		setBackground(Color.WHITE);
	}

	@Override
	public void update(Observable obj, Object arg) {
		repaint();
	}

	private void drawCell(Graphics g, int x, int y) {
		g.setColor(Color.BLACK);
		g.fillRect(x * resolution, y * resolution, resolution, resolution);
		g.setColor(Color.GREEN);
		g.fillRect(x * resolution + border, y * resolution + border, resolution
				- 2 * border, resolution - 2 * border);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < model.getWidth(); ++i) {
			for (int j = 0; j < model.getHeight(); ++j) {
				if (model.getCell(i, j)) {
					drawCell(g, i, j);
				}
			}
		}
	}
}
