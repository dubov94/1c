package tetris;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Observable;
import javax.swing.Timer;

public class Model extends Observable {
	private final Dimension dimension;
	private final boolean field[][];
	private int score;
	private Timer timer;
	private int delay = 500;
	private State state = State.OVER;
	private FigureDriver driver;

	private enum State {
		OVER, CREATE, MOVE
	}

	public Model(Dimension dimension) {
		this.dimension = dimension;
		field = new boolean[dimension.width][dimension.height];
	}

	public int getWidth() {
		return dimension.width;
	}

	public int getHeight() {
		return dimension.height;
	}

	private void startTimer() {
		timer = new Timer(delay, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (state == State.CREATE) {
					driver = FigureFactory.getRandom(dimension.width);
					state = State.MOVE;
				}
				if (state == State.MOVE) {
					if (!creep()) {
						if (driver.point.height - driver.figure.pivot.height < 0) {
							timer.stop();
							state = State.OVER;
							update();
						} else {
							state = State.CREATE;
						}
					}
				}
			}
		});
		timer.start();
	}

	public void restart() {
		if (timer != null) {
			timer.stop();
		}
		score = 0;
		for (boolean[] column : field) {
			Arrays.fill(column, false);
		}
		state = State.CREATE;
		startTimer();
		update();
	}

	public boolean getCell(int x, int y) {
		return field[x][y];
	}

	public int getScore() {
		return score;
	}

	public boolean isGameOver() {
		return state == State.OVER;
	}

	private void update() {
		setChanged();
		notifyObservers();
	}

	private int getFieldCellX(int dx) {
		return driver.point.width - driver.figure.pivot.width + dx;
	}

	private int getFieldCellY(int dy) {
		return driver.point.height - driver.figure.pivot.height + dy;
	}

	private boolean doesCurrentCellOverflow(int dx, int dy) {
		return getFieldCellX(dx) >= dimension.width || getFieldCellX(dx) < 0
				|| getFieldCellY(dy) >= dimension.height;
	}

	private void setCellSafeCeil(int x, int y, boolean value) {
		if (y >= 0) {
			field[x][y] = value;
		}
	}

	private boolean getCellSafeCeil(int x, int y) {
		if (y >= 0) {
			return field[x][y];
		} else {
			return false;
		}
	}

	private void clearCurrentPosition() {
		for (int i = 0; i < driver.figure.size.width; ++i) {
			for (int j = 0; j < driver.figure.size.height; ++j) {
				if (driver.figure.mask[i][j] && !doesCurrentCellOverflow(i, j)) {
					setCellSafeCeil(getFieldCellX(i), getFieldCellY(j), false);
				}
			}
		}
	}

	private void printCurrentPosition(boolean inform) {
		for (int i = 0; i < driver.figure.size.width; ++i) {
			for (int j = 0; j < driver.figure.size.height; ++j) {
				if (driver.figure.mask[i][j] && !doesCurrentCellOverflow(i, j)) {
					setCellSafeCeil(getFieldCellX(i), getFieldCellY(j),
							driver.figure.mask[i][j]);
				}
			}
		}
		if (inform)
			update();
	}

	private boolean isCurrentPositionValid() {
		for (int i = 0; i < driver.figure.size.width; ++i) {
			for (int j = 0; j < driver.figure.size.height; ++j) {
				if (doesCurrentCellOverflow(i, j)
						|| getCellSafeCeil(getFieldCellX(i), getFieldCellY(j))
						&& driver.figure.mask[i][j])
					return false;
			}
		}
		return true;
	}

	public void moveLeft() {
		if (state == State.MOVE) {
			clearCurrentPosition();
			int oldX = driver.point.width--;
			if (!isCurrentPositionValid()) {
				driver.point.width = oldX;
			}
			printCurrentPosition(true);
		}
	}

	public void moveRight() {
		if (state == State.MOVE) {
			clearCurrentPosition();
			int oldX = driver.point.width++;
			if (!isCurrentPositionValid()) {
				driver.point.width = oldX;
			}
			printCurrentPosition(true);
		}
	}

	public void moveUp() {
		if (state == State.MOVE) {
			clearCurrentPosition();
			driver.figure.rotate();
			if (!isCurrentPositionValid()) {
				for (int i = 0; i < 3; ++i) {
					driver.figure.rotate();
				}
			}
			printCurrentPosition(true);
		}
	}

	private void clearEmptyRows() {
		for (int i = dimension.height - 1; i >= 0; --i) {
			boolean rowCleared;
			do {
				rowCleared = true;
				for (int j = 0; j < dimension.width; ++j) {
					if (!field[j][i]) {
						rowCleared = false;
						break;
					}
				}
				if (rowCleared) {
					for (int a = i - 1; a >= 0; --a) {
						for (int b = 0; b < dimension.width; ++b) {
							field[b][a + 1] = field[b][a];
						}
					}
					for (int b = 0; b < dimension.width; ++b) {
						field[b][0] = false;
					}
					++score;
				}
			} while (rowCleared);
		}
	}

	private boolean creep() {
		clearCurrentPosition();
		int oldY = driver.point.height++;
		if (!isCurrentPositionValid()) {
			driver.point.height = oldY;
			printCurrentPosition(false);
			clearEmptyRows();
			update();
			return false;
		} else {
			printCurrentPosition(true);
			return true;
		}
	}

	public void moveDown() {
		if (state == State.MOVE) {
			timer.stop();
			while (creep())
				;
			state = State.CREATE;
			startTimer();
		}
	}
}
