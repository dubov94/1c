package tetris;

import java.awt.Dimension;
import java.util.Random;

public class FigureFactory {
	private static final Random random = new Random();
	private static final String[] figures;
	static {
		figures = new String[] { "1\n1\n2\n1\n1", "121\n101", "010\n121",
				"21\n10", "100\n121\n001", "11\n12\n11", "10\n21\n10\n10" };
	}

	private static FigureDriver.Figure getFigure(int index) {
		String repr = figures[index];
		String[] split = repr.split("\n");
		Dimension size = new Dimension(split[0].length(), split.length);
		Dimension pivot = null;
		boolean[][] mask = new boolean[size.width][size.height];
		for (int j = 0; j < size.height; ++j) {
			for (int i = 0; i < size.width; ++i) {
				char c = split[j].charAt(i);
				int key = c - '0';
				mask[i][j] = (key > 0);
				if (key == 2) {
					pivot = new Dimension(i, j);
				}
			}
		}
		assert (pivot != null);
		return new FigureDriver.Figure(mask, size, pivot);
	}

	public static FigureDriver getRandom(int width) {
		FigureDriver driver = new FigureDriver();
		driver.figure = getFigure(random.nextInt(figures.length));
		driver.point = new Dimension(random.nextInt(width
				- driver.figure.size.width + 1)
				+ driver.figure.pivot.width,
				-(driver.figure.size.height - driver.figure.pivot.height));
		return driver;
	}
}
