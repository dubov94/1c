package tetris;

import java.awt.Dimension;

public class FigureDriver {
	public static class Figure {
		public Dimension size, pivot;
		public boolean[][] mask;

		public Figure(boolean[][] mask, Dimension size, Dimension pivot) {
			this.size = size;
			this.pivot = pivot;
			this.mask = mask;
		}

		public void rotate() {
			Dimension rPivot = new Dimension(pivot.height, size.width - 1
					- pivot.width);
			Dimension rSize = new Dimension(size.height, size.width);
			boolean[][] rMask = new boolean[rSize.width][rSize.height];
			for (int i = 0; i < size.width; ++i) {
				for (int j = 0; j < size.height; ++j) {
					rMask[j - pivot.height + rPivot.width][pivot.width - i
							+ rPivot.height] = mask[i][j];
				}
			}
			this.size = rSize;
			this.pivot = rPivot;
			this.mask = rMask;
		}
	}

	public Figure figure;
	public Dimension point;
}
