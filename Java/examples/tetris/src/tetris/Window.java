package tetris;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Window extends JFrame {
	public Window() {
		Container container = getContentPane();
		Model model = new Model(new Dimension(15, 20));
		container.add(new MainPanel(model));
		setTitle("������");
		setResizable(false);
		setFocusable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dimension.width / 2 - this.getSize().width / 2,
				dimension.height / 2 - this.getSize().height / 2);
		setVisible(true);
	}
}
