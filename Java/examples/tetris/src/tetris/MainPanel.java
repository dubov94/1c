package tetris;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class MainPanel extends JPanel implements Observer {
	private final JLabel scoreLabel;
	private final Model model;

	public MainPanel(Model model) {
		this.model = model;
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		View view = new View(model);
		model.addObserver(view);
		model.addObserver(this);
		constraints.gridy = 0;
		add(view, constraints);
		JPanel controls = new JPanel();
		JButton newGameButton = new JButton("����� ����");
		newGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.restart();
			}
		});
		controls.add(newGameButton);
		JButton exitButton = new JButton("�����");
		exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		controls.add(exitButton);
		scoreLabel = new JLabel("0");
		controls.add(scoreLabel);
		constraints.gridy = 1;
		add(controls, constraints);
		InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke("UP"), "up");
		inputMap.put(KeyStroke.getKeyStroke("DOWN"), "down");
		inputMap.put(KeyStroke.getKeyStroke("RIGHT"), "right");
		inputMap.put(KeyStroke.getKeyStroke("LEFT"), "left");
		getActionMap().put("up", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.moveUp();
			}
		});
		getActionMap().put("down", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.moveDown();
			}
		});
		getActionMap().put("left", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.moveLeft();
			}
		});
		getActionMap().put("right", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.moveRight();
			}
		});
	}

	@Override
	public void update(Observable o, Object arg) {
		if (model.isGameOver()) {
			JOptionPane.showMessageDialog(null,
					String.format("�� ������� %d!", model.getScore()));
		} else {
			scoreLabel.setText(String.valueOf(model.getScore()));
		}
		repaint();
	}
}
