package clock;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
class Plate extends JPanel {
	int greenTime, blueTime;
	Timer timer;
	final int shift = 50, radius;
	final int initialDelay = 500;
	final int mainCircleCenterX, mainCircleCenterY;
	
	public Plate(int windowSize) {
		radius = (windowSize - 2 * shift) / 2;
		mainCircleCenterX = radius + shift;
		mainCircleCenterY = mainCircleCenterX;
		greenTime = 0;
		blueTime = 0;
		timer = new Timer(initialDelay, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				greenTime = (greenTime + 1) % 120;
				blueTime = (blueTime - 2 + 120) % 120; 
				if(greenTime == blueTime) {
					Timer self = Plate.this.timer;
					self.setDelay(self.getDelay() / 2);
				}
				Plate.this.repaint();
			}
		});
		timer.start();
	}

	private void drawOutLine(Graphics g) {
		g.setColor(Color.RED);
		g.drawOval(shift, shift, radius * 2, radius * 2);
	}
	
	private class ArrowShift {
		private double baseCircleX, baseCircleY;
		public ArrowShift(int time) {
			double angle = -2 * Math.PI / 60 * time - Math.PI / 2;
			baseCircleX = Math.cos(angle);
			baseCircleY = -Math.sin(angle);
		}
		
		public double getBaseCircleX() {
			return baseCircleX;
		}
		
		public double getBaseCircleY() {
			return baseCircleY;
		}
	}
	
	private void drawMarking(Graphics g) {
		g.setColor(Color.BLACK);
		for (int i = 0; i < 60; i++) {
			ArrowShift arrowShift = new ArrowShift(i);
			double mainCircleShiftX = radius * arrowShift.getBaseCircleX(),
					mainCircleShiftY = radius * arrowShift.getBaseCircleY();
			g.drawLine(
					(int)(mainCircleCenterX - 0.9 * mainCircleShiftX), 
					(int)(mainCircleCenterY - 0.9 * mainCircleShiftY), 
					(int)(mainCircleCenterX - mainCircleShiftX), 
					(int)(mainCircleCenterY - mainCircleShiftY)
			);
		}
	}

	private void drawArrow(Graphics g, int time, Color c) {
		g.setColor(c);
		ArrowShift arrowShift = new ArrowShift(time);
		g.drawLine(mainCircleCenterX, mainCircleCenterY,
				(int)(mainCircleCenterX - radius * arrowShift.getBaseCircleX()),
				(int)(mainCircleCenterY - radius * arrowShift.getBaseCircleY()));
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawOutLine(g);
		drawMarking(g);
		drawArrow(g, greenTime / 2, Color.GREEN);
		drawArrow(g, blueTime / 2, Color.BLUE);
	}
}
