package clock;

import java.awt.Container;

import javax.swing.JFrame;

@SuppressWarnings("serial")
class Window extends JFrame
{
	final int windowSize = 600;
	public Window()
	{
	  
		Plate plate = new Plate(windowSize);
		Container container = getContentPane();
		container.add(plate);
		setBounds(0, 0, windowSize, windowSize);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}	
}