import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Listener implements KeyListener {
	Panel p;
	
	Listener(Panel panel) {
		p = panel;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		double dx = p.get_scale(0.3);
		if(code == KeyEvent.VK_LEFT) {
			p.car_dx = -dx;
		} else if(code == KeyEvent.VK_RIGHT) {
			p.car_dx = dx;
		} else if(code == KeyEvent.VK_ESCAPE) {
			System.exit(0);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		p.car_dx = 0;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

}
