import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Panel extends JPanel implements ActionListener {
	int width, height;
	Image car_left, car_right, ally_car, road, explosion;
	double road_y, road_w, road_x, road_v, explosion_h;
	double car_dx, car_x, car_w, car_h, car_offset;
	int max_cars;
	Car[] cars;
	Timer t;
	double past;
	
	Panel(int w, int h) {
		width = w;
		height = h;
		try {
			ally_car = ImageIO.read(new File("orange_car.png"));
			car_left = ImageIO.read(new File("blue_car_left.png"));
			car_right = ImageIO.read(new File("blue_car_right.png"));
			road = ImageIO.read(new File("road.jpg"));
			explosion = ImageIO.read(new File("explosion.png"));
		} catch(IOException e){
			System.out.println(e.getMessage());
		}
		past = 0;
		road_v = 0.5;
		car_dx = 0;
		car_offset = width / 36.0;
		car_x = car_offset;
		car_w = width / 9.0;
		car_h = height / 6.0;
		road_w = width / 3.0;
		road_x = (width - road_w) / 2;
		road_y = -height;
		explosion_h = 200;
		max_cars = (int)((height + car_h) / (double)(car_w + car_h));
		cars = new Car[max_cars];
		for(int i = 0; i < max_cars; i = i + 1) {
			cars[i] = new Car();
		}
		cars[0].left = Math.random() > 0.5;
		cars[0].y = -car_w;
		for(int i = 1; i < max_cars; i = i + 1) {
			cars[i].generate(cars[i - 1].y, car_h);
		}
		t = new Timer(1, this);
		t.start();
	}
	
	void draw(Graphics g, Image i, double x, double y, double w, double h) {
		g.drawImage(i, (int)x, (int)y, (int)w, (int)h, null);
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, width, height);
		draw(g, road, road_x, road_y, road_w, height * 2);
		for(int i = 0; i < max_cars; i = i + 1) {
			if(cars[i].left) {
				draw(g, car_right, road_x - car_h / 2, cars[i].y, car_h, car_w);
			} else {
				draw(g, car_left, road_x + road_w - car_h / 2, cars[i].y, car_h, car_w);
				
			}
		}
		draw(g, ally_car, road_x + car_x, (height - car_h) / 2, car_w, car_h);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Serif", Font.PLAIN, 36));
		g.drawString(String.valueOf(Math.round(past)), 20, 50);
		if(!t.isRunning()) {
			draw(g, explosion, road_x + car_x + car_w / 2 - road_w / 2, height / 2.0 - explosion_h * 3 / 4, road_w, explosion_h);
		}
	}
	
	double get_scale(double base) {
		return base + past / 1000 * base;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		road_v = get_scale(0.5);
		road_y = road_y + road_v;
		past = past + road_v / car_w * 1.5;
		car_x += car_dx;
		car_x = Math.min(car_x, road_w - car_offset - car_w);
		car_x = Math.max(car_x, car_offset);
		if(road_y >= 0) {
			road_y = -height;
		}
		for(int i = 0; i < max_cars; i = i + 1) {
			double cur_car_top = cars[i].y, cur_car_bot = cars[i].y + car_w;
			double ally_car_top = (height - car_h) / 2, ally_car_bot = ally_car_top + car_h;
			double cur_car_x;
			boolean x_intersect = false;
			if(cars[i].left) {
				cur_car_x = road_x + car_h / 2;
				if(cur_car_x > road_x + car_x) {
					x_intersect = true;
				}
			} else {
				cur_car_x = road_x + road_w - car_h / 2;
				if(cur_car_x < road_x + car_x + car_w) {
					x_intersect = true;
				}
			}
			if(x_intersect && ally_car_top < cur_car_bot && ally_car_bot > cur_car_top) {
				t.stop();
			}
			cars[i].y = cars[i].y + road_v;
			if(cars[i].y >= height) {
				if(i == 0) {
					cars[i].generate(cars[max_cars - 1].y, car_h);
				} else {
					cars[i].generate(cars[i - 1].y, car_h);
				}
			}
		}
		repaint();
	}
}
