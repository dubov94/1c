import javax.swing.JFrame;

public class Window extends JFrame {
	int width = 500, height = 700;
	
	Window() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, width, height);
		Panel p = new Panel(width, height);
		Listener l = new Listener(p);
		addKeyListener(l);
		add(p);
		setTitle("Московские парковки");
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		Window w = new Window();
	}
}
