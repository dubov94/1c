import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Panel extends JPanel implements ActionListener {
	int width, height, cell_size = 20;
	int map_w = 21, map_h = 22;
	int[][] map = new int[map_w][map_h];
	Pacman pacman;
	double v = 0.2;
	int score = 0;
	int max_score = 0;
	int[][] teleport = new int[2][2];
	int ghosts_count = 5;
	Ghost[] ghosts = new Ghost[ghosts_count];
	double eps = 1e-9;
	Timer t;
	
	
	Panel(Window win) {
		width = map_w * cell_size;
		height = map_h * cell_size;
		win.setSize(width + 10, height + 35);
		try {
			Scanner s = new Scanner(new File("map.txt"));
			int t = 0, g = 0;
			for(int h = 0; h < map_h; h = h + 1) {
				String line = s.nextLine();
				for(int w = 0; w < map_w; w = w + 1) {
					map[w][h] = line.charAt(w) - '0'; // '0', '1'
					if(map[w][h] == 0) {
						max_score = max_score + 1;
					} else if(map[w][h] == 3) {
						teleport[t][0] = w;
						teleport[t][1] = h;
						t = t + 1;
					} else if(map[w][h] == 4) {
						ghosts[g] = new Ghost();
						ghosts[g].x = w * cell_size;
						ghosts[g].y = h * cell_size;
						ghosts[g].c = new Color((int)Math.round(Math.random() * (1 << (8 * 3))));
						ghosts[g].dx = -1;
						ghosts[g].dy = 0;
						g = g + 1;
					}
				}
			}
		} catch(IOException e) {
			System.out.println(e.getMessage());
		}
		pacman = new Pacman();
		pacman.x = cell_size;
		pacman.y = cell_size;
		pacman.wish_dx = 0;
		pacman.wish_dy = 0;
		t = new Timer(1, this);
		t.start();
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		for(int h = 0; h < map_h; h = h + 1) {
			for(int w = 0; w < map_w; w = w + 1) {
				if(map[w][h] == 1) {
					g.setColor(Color.BLUE);
					g.fillRect(w * cell_size, h * cell_size, cell_size, cell_size);
				} else if(map[w][h] == 0) {
					g.setColor(Color.WHITE);
					g.fillOval(w * cell_size + cell_size / 2, h * cell_size + cell_size / 2, 2, 2);
				}
			}
		}
		for(int i = 0; i < ghosts_count; i = i + 1) {
			g.setColor(ghosts[i].c);
			g.fillOval((int)ghosts[i].x, (int)ghosts[i].y, cell_size, cell_size);
		}
		g.setColor(Color.YELLOW);
		g.fillOval((int)pacman.x, (int)pacman.y, cell_size, cell_size);
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 16));
		g.drawString(String.valueOf(score), 3, 17);
	}

	boolean intersect(double x1, double y1, double x2, double y2) {
		if(x1 + cell_size < x2
				|| x2 + cell_size < x1
				|| y1 + cell_size < y2
				|| y2 + cell_size < y1) {
			return false;
		} else {
			return true;
		}
	}
	
	boolean blocked(int xc, int yc) {
		if(xc < 0 || xc >= map_w || yc < 0 || yc >= map_h || map[xc][yc] == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	boolean in_corner(Ball b) {
		double xc = b.x / cell_size, yc = b.y / cell_size;
		return Math.abs(xc - Math.round(xc)) < eps && Math.abs(yc - Math.round(yc)) < eps;
	}
	
	int get_X(Ball b) {
		return (int)Math.round(b.x / cell_size);
	}
	
	int get_Y(Ball b) {
		return (int)Math.round(b.y / cell_size);
	}
	
	void move(Ball b) {
		b.x = b.x + b.dx * v;
		b.y = b.y + b.dy * v;
	}
	
	void tele(Ball b) {
		int X = get_X(b), Y = get_Y(b);
		for(int t = 0; t < 2; t = t + 1) {
			if(teleport[t][0] == X && teleport[t][1] == Y) {
				X = teleport[1 - t][0];
				Y = teleport[1 - t][1];
				b.x = X * cell_size;
				b.y = Y * cell_size;
				break;
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		for(int i = 0; i < ghosts_count; i = i + 1) {
			if(in_corner(ghosts[i])) {
				tele(ghosts[i]);
				int X = get_X(ghosts[i]), Y = get_Y(ghosts[i]);
				int[][] dir = new int[4][2];
				int[][] directions = new int[][] { {1, 0}, {-1, 0}, {0, 1}, {0, -1} };
				int available = 0;
				for(int j = 0; j < 4; j = j + 1) {
					if(!blocked(X + directions[j][0], Y + directions[j][1]) && 
							!(directions[j][0] == -ghosts[i].dx && directions[j][1] == -ghosts[i].dy)) {
						dir[available][0] = directions[j][0];
						dir[available][1] = directions[j][1];
						available = available + 1;
					}
				}
				if(blocked(X + ghosts[i].dx, Y + ghosts[i].dy) || available > 1) {
					if(available == 0) {
						ghosts[i].dx = -ghosts[i].dx;
						ghosts[i].dy = -ghosts[i].dy;
					} else {
						int chosen = (int)(Math.random() * available);
						ghosts[i].dx = dir[chosen][0];
						ghosts[i].dy = dir[chosen][1];
					}
				}
			}
			move(ghosts[i]);
		}
		
		if(in_corner(pacman)) {
			tele(pacman);
			int X = get_X(pacman), Y = get_Y(pacman);
			if(!blocked(X + pacman.wish_dx, Y + pacman.wish_dy)) { 
				pacman.dx = pacman.wish_dx;
				pacman.dy = pacman.wish_dy;
			} else if(blocked(X + pacman.dx, Y + pacman.dy)) {
				pacman.dx = 0;
				pacman.dy = 0;
			}
			
		}
		move(pacman);
		
		for(int i = 0; i < ghosts_count; i = i + 1) {
			if(intersect(pacman.x, pacman.y, ghosts[i].x, ghosts[i].y)) {
				JOptionPane.showMessageDialog(null, "You lost!");
				System.exit(0);
			}
		}
		
		int pw = (int)Math.floor(pacman.x / cell_size + 0.5), 
				ph = (int)Math.floor(pacman.y / cell_size + 0.5);
		if(map[pw][ph] == 0) {
			map[pw][ph] = 2;
			score = score + 1;
		}
		repaint();
		if(score == max_score) {
			JOptionPane.showMessageDialog(null, "You won!");
			System.exit(0);
		}
	}
}
