import javax.swing.JFrame;

public class Window extends JFrame {
	Window() {
		Panel p = new Panel(this);
		add(p);
		Listener l = new Listener(p);
		addKeyListener(l);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(0, 0);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		Window w = new Window();
	}
}
