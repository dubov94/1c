import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Listener implements KeyListener {
	Panel panel;
	
	Listener(Panel p) {
		panel = p;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if(code == KeyEvent.VK_LEFT) {
			panel.pacman.wish_dx = -1;
			panel.pacman.wish_dy = 0;
		} else if(code == KeyEvent.VK_RIGHT) {
			panel.pacman.wish_dx = 1;
			panel.pacman.wish_dy = 0;
		} else if(code == KeyEvent.VK_UP) {
			panel.pacman.wish_dx = 0;
			panel.pacman.wish_dy = -1;
		} else if(code == KeyEvent.VK_DOWN) {
			panel.pacman.wish_dx = 0;
			panel.pacman.wish_dy = 1;
		} else if(code == KeyEvent.VK_SPACE) {
			panel.pacman.wish_dx = 0;
			panel.pacman.wish_dy = 0;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
	
}
